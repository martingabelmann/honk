# containerized honk

how to setup [honk](https://humungus.tedunangst.com/r/honk) using docker:

```bash
# docker build . -t honk
# docker run -v $PWD/honk:/db -ti --rm honk init
username: admin
password: ****
listen address: 0.0.0.0:8080
server name: honk.domain.com
done.

# docker run --name honk -v $PWD/honk:/db -d -p 8080:8080 honk
```
you may want to use in combination with a reverse proxy!
