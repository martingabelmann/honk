FROM alpine:latest

RUN apk update && apk add \
    mercurial make go sqlite-dev &&\
    adduser -h /honk -s /bin/sh -u 1000 -D honk &&\
    hg clone https://humungus.tedunangst.com/r/honk /tmp/honk &&\
    cd /tmp/honk && make && cp honk /honk/ && cp -r views /honk/ &&\
    apk del mercurial make go && mkdir /db && chown honk /db && rm -rf /tmp/honk

USER honk
WORKDIR /honk
VOLUME /db
ENTRYPOINT ["/honk/honk", "--datadir=/db", "--viewdir=/honk"]
